def is_armstrong(number):
    nb_digits = len(str(number)) #Nombre de digits du nombre
    amstrong = 0
    for digit in str(number):
        amstrong += int(digit)**nb_digits
    
    return number == amstrong

